
package com.v2maestros.spark.bda.train;

import java.util.*;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.classification.DecisionTreeClassificationModel;
import org.apache.spark.ml.classification.DecisionTreeClassifier;
import org.apache.spark.ml.classification.NaiveBayes;
import org.apache.spark.ml.classification.RandomForestClassificationModel;
import org.apache.spark.ml.classification.RandomForestClassifier;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;

import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator;
import org.apache.spark.ml.evaluation.RegressionEvaluator;
import org.apache.spark.ml.feature.*;
import org.apache.spark.ml.linalg.*;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.ml.regression.LinearRegressionModel;
import org.apache.spark.ml.regression.LinearRegressionTrainingSummary;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.util.DoubleAccumulator;
import org.apache.spark.util.LongAccumulator;

import static org.apache.spark.sql.functions.*;

import com.v2maestros.spark.bda.common.ExerciseUtils;
import com.v2maestros.spark.bda.common.SparkConnection;
import org.apache.spark.api.java.JavaRDD;
import scala.Tuple2;
import scala.runtime.BoxedUnit;


public class Project
{

    public static SparseVector merge(SparseVector a,SparseVector b) {

        TreeMap<Integer,Double> tm = new TreeMap<>() ;

        int[] ind = a.indices() ;
        double[] vals = a.values() ;
      //  int idx = 0;
        for(int i = 0 ; i < ind.length ; i++) {
        //    idx = Math.max(idx,ind[i] + 1);
            if(!tm.containsKey(ind[i])) tm.put(ind[i],0.0) ;
            tm.put(ind[i],1.0) ;
        }

        ind = b.indices() ;
        vals = b.values() ;

        for(int i = 0 ; i < ind.length ; i++) {
          //  idx = Math.max(idx,ind[i] + 1);
            if(!tm.containsKey(ind[i])) tm.put(ind[i],0.0) ;
            tm.put(ind[i],1.0 ) ;
        }

        ind = new int[tm.size()] ;
        vals = new double[tm.size()] ;
        int i = 0 ;
        for(Map.Entry<Integer,Double> x : tm.entrySet()) {
            ind[i] = x.getKey() ;
            vals[i++] = x.getValue() ;
        }

        return new SparseVector(a.size(),ind,vals) ;
    }

    public static void main(String[] args) {
/*
        Vector dv1 = Vectors.sparse(2, new int[] {0, 2}, new double[] {1.0, 3.0});
        Vector dv2 = Vectors.sparse(12, new int[] {10, 11}, new double[] {4.0, 3.0});

        SparseVector a = merge(dv1.toSparse(),dv2.toSparse());

        System.out.println(a);
*/


        Logger.getLogger("org").setLevel(Level.ERROR);
        Logger.getLogger("akka").setLevel(Level.ERROR);
        JavaSparkContext spContext = SparkConnection.getContext();
        SparkSession spSession = SparkConnection.getSession();

        Dataset<Row> autoDf2 = spSession.read()
                .option("header", "true")
                .csv("/Users/sandeep.ku/PycharmProjects/demo/data.csv");



        autoDf2.show();

        JavaRDD<Row> t = autoDf2.toJavaRDD();

        JavaRDD<Row> tra = t.flatMap(new FlatMapFunction<Row, Row>() {
            @Override
            public Iterator<Row> call(Row iRow) throws Exception {

                List<Row> v = new ArrayList<Row>();

                String[] str = iRow.getString(1).split("/");

             //   System.out.println(str[1]);

                String temp = "";

                for(int i = 0;i < str.length;i++) {
                    temp += str[i];
                    v.add(RowFactory.create(iRow.getString(0), temp));
                }


                return v.iterator();
            }

        });

        System.out.println(tra.count());

        StructType autoSchema = DataTypes
                .createStructType(new StructField[] {
                        DataTypes.createStructField("id", DataTypes.StringType, false, Metadata.empty()),
                        DataTypes.createStructField("category", DataTypes.StringType, false,Metadata.empty()),
                });


        Dataset<Row> autoDf = spSession.createDataFrame(tra, autoSchema);

        autoDf.show();



     //   autoDf.show(5);
     //   autoDf.printSchema();
        System.out.println(autoDf.count());


        StringIndexerModel indexer = new StringIndexer()
                .setInputCol("category")
                .setOutputCol("categoryIndex")
                .fit(autoDf);

        Dataset<Row> indexed = indexer.transform(autoDf);

        OneHotEncoder encoder = new OneHotEncoder()
                .setInputCol("categoryIndex")
                .setOutputCol("categoryVec");

        Dataset<Row> encoded = encoder.transform(indexed);
        encoded.show();

        encoded.printSchema();

        JavaRDD<Row> rdd1 = encoded.toJavaRDD();

        JavaPairRDD<String,Vector> abhi = rdd1.mapToPair(r -> {
            return new Tuple2<String,Vector>(r.getString(0),(Vector)r.get(3)) ;
        }) ;

        abhi  = abhi.reduceByKey((v1,v2) -> merge(v1.toSparse(),v2.toSparse()));




     //   System.out.println(abhi.collect());

//        System.out.println(abhi.count());

        JavaRDD<Row> temp = abhi.map(new Function<Tuple2<String, Vector>, Row>() {
            @Override
            public Row call(Tuple2<String, Vector> stringVectorTuple2) throws Exception {

                return RowFactory.create(stringVectorTuple2._1(),stringVectorTuple2._2());
            }
        });


        StructType schema = DataTypes
                .createStructType(new StructField[] {
                        DataTypes.createStructField("label", DataTypes.StringType, false, Metadata.empty()),
                        DataTypes.createStructField("features", new VectorUDT(), false,Metadata.empty()),
                });


        Dataset<Row> ml = spSession.createDataFrame(temp,schema);

        ml = ml.select("features");

        ml.printSchema();

        KMeans kmeans = new KMeans().setK(50).setSeed(1L).setMaxIter(1000);
        KMeansModel model = kmeans.fit(ml);

        double WSSSE = model.computeCost(ml);
        System.out.println("Within Set Sum of Squared Errors = " + WSSSE);

     //   System.out.println(abhi.take(10));



    }

}